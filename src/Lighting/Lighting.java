package Lighting;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.RadialGradientPaint;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import Game.Elements.Element;

public class Lighting
{
	protected static float gradSize;

	protected static float[] gradFrac;

	protected Color[] gradColors;

	protected static Polygon umbra;

	//public List<Element> shapes;
	protected List<ArrayList<Element>> subShapes;
	
	public Lighting(float lightRadius)
	{
		gradSize = lightRadius;
		gradFrac = new float[]
		{ 0f, 1f };
		gradColors = new Color[]
		{ Color.black, new Color(0f, 0f, 0f, 0f) };
		umbra = new Polygon();
		subShapes = new ArrayList<ArrayList<Element>>();
	}

	public void renderShadows(Graphics2D g, ArrayList<Element> shapes,float mouseX, float mouseY)
	{
		Paint oldPaint = g.getPaint();

		float minDistSq = gradSize * gradSize;

		final Point2D.Float mouse = new Point2D.Float(mouseX, mouseY);
		Paint gradientPaint;

		for (int i = 0; i < shapes.size(); i++)
		{
			Shape e = shapes.get(i).shape;

			gradColors[0] = shapes.get(i).color;
			
			gradientPaint = new RadialGradientPaint(
					new Point2D.Float(mouseX, mouseY), gradSize, gradFrac,
					gradColors);

			Rectangle2D bounds = e.getBounds2D();

			float r = (float) bounds.getWidth() / 2f;

			float cyl_yl_x = (float) bounds.getX() + r;
			float cyl_y = (float) bounds.getY() + r;

			float off_x = cyl_yl_x - mouse.x;
			float off_y = cyl_y - mouse.y;

			float distSq = off_x * off_x + off_y * off_y;

			if (distSq > minDistSq)
				continue;

			float length = (float) Math.sqrt(distSq);
			float null_x = off_x;
			float null_y = off_y;

			if (length != 0)
			{
				null_x /= length;
				null_y /= length;
			}

			float px = -null_y;
			float py = null_x;

			Point2D.Float lightOrig = new Point2D.Float(cyl_yl_x - px * r,
					cyl_y - py * r);
			Point2D.Float secLightOrig = new Point2D.Float(cyl_yl_x + px * r,
					cyl_y + py * r);

			Point2D.Float firstEx = extrude(mouse, lightOrig, minDistSq);
			Point2D.Float secEx = extrude(mouse, secLightOrig, minDistSq);

			umbra.reset();
			umbra.addPoint((int) lightOrig.x, (int) lightOrig.y);
			umbra.addPoint((int) secLightOrig.x, (int) secLightOrig.y);
			umbra.addPoint((int) secEx.x, (int) secEx.y);
			umbra.addPoint((int) firstEx.x, (int) firstEx.y);

			g.setPaint(gradientPaint);
			g.fillPolygon(umbra);
		}

		g.setPaint(oldPaint);
	}

	/*private void subdivide(List<Element> shapes, List<List<Element>> subShapes)
	{
		for (int i = 0; i < shapes.size(); i++)
		{
			//if (shapes.get(i).shape.getBounds2D().getWidth() / )
		}
	}*/
	
	private Point2D.Float extrude(Point2D.Float beg, Point2D.Float end,
			float scalar)
	{
		float off_x = end.x - beg.x;
		float off_y = end.y - beg.y;
		float length = (float) Math.sqrt(off_x * off_x + off_y * off_y);
		if (length != 0)
		{
			off_x /= length;
			off_y /= length;
		}
		off_x *= scalar;
		off_y *= scalar;
		return new Point2D.Float(end.x + off_x, end.y + off_y);
	}
}
