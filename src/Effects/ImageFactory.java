package Effects;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;

import javax.imageio.ImageIO;

public class ImageFactory
{

	public static Image errorImage;
	public static Image qMark;
	public static Image levelImage;
	public static Image bgMask;
	public static Image icon;
	
	public static Image extraLifePowerUp;
	public static Image enlargePowerUp;
	public static Image shrinkPowerUp;
	public static Image fastenPowerUp;
	public static Image slowdownPowerUp;
	public static Image explosionPowerUp;
	
	
	static
	{
		try
		{
			errorImage = ImageIO.read( new File("res/images/icons/error.png")).getScaledInstance(64, 64, Image.SCALE_SMOOTH);
			qMark = ImageIO.read(new File("res/images/icons/qmark.png")).getScaledInstance(64, 64, Image.SCALE_SMOOTH);
			Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
			levelImage = ImageIO.read(new File("res/images/level.png")).getScaledInstance(size.width, size.height, Image.SCALE_SMOOTH);
			bgMask = ImageIO.read(new File("res/images/bgMask.png")).getScaledInstance(400, 400, Image.SCALE_SMOOTH);
		
			extraLifePowerUp = ImageIO.read(new File("res/images/power-ups/extra-life-icon.png")).getScaledInstance(30, 30, Image.SCALE_SMOOTH);
			enlargePowerUp = ImageIO.read(new File("res/images/power-ups/enlarge-icon.png")).getScaledInstance(30, 30, Image.SCALE_SMOOTH);
			shrinkPowerUp = ImageIO.read(new File("res/images/power-ups/shrink-icon.png")).getScaledInstance(30, 30, Image.SCALE_SMOOTH);
			fastenPowerUp = ImageIO.read(new File("res/images/power-ups/ffw-icon.png")).getScaledInstance(30, 30, Image.SCALE_SMOOTH);
			slowdownPowerUp = ImageIO.read(new File("res/images/power-ups/ffb-icon.png")).getScaledInstance(30, 30, Image.SCALE_SMOOTH);
			explosionPowerUp = ImageIO.read(new File("res/images/power-ups/bomb-icon.png")).getScaledInstance(30, 30, Image.SCALE_SMOOTH);

		
		}
		catch(Exception e)
		{
			System.out.println("Couldn't load error image");
		}
	}
}