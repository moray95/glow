package Effects;

import java.io.File;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;

import Menus.PreferencesController;

public class Sound
{

	public static Sound music = new Sound("music.wav");
	public static Sound mouseHoover = new Sound("hoover.wav");
	public static Sound mouseClick = new Sound("click.wav");
	
	public static Sound brickHit = new Sound("brick_hit.wav");
	public static Sound explosion = new Sound("explosion.wav");
	
	public static Sound[] soundEffects = {mouseHoover, mouseClick, brickHit, explosion};

	public static void init()
	{
		PreferencesController prefs = new PreferencesController();
		music.setVolume ((float) prefs.getMusicVolume());
		mouseHoover.setVolume((float) prefs.getSoundEffectVolume());
		mouseClick.setVolume( (float) prefs.getSoundEffectVolume());
		
		brickHit.setVolume((float) prefs.getSoundEffectVolume());
		explosion.setVolume((float) prefs.getSoundEffectVolume());
	}


	Clip clip;
	String url;
	private float volume;

	private Sound(String name)
	{
		
		url = "res/sounds/" + name;
		
	}

	public void play()
	{
		try
		{

			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(url.toString()));
			AudioFormat af = audioInputStream.getFormat();
			int size = (int) (af.getFrameSize() * audioInputStream.getFrameLength());
			byte[] audio = new byte[size];
			DataLine.Info info = new DataLine.Info(Clip.class, af, size);
			audioInputStream.read(audio, 0, size);

			Clip clip = (Clip) AudioSystem.getLine(info);
			clip.open(af, audio, 0, size);
			
			try
			{
				FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
				volume.setValue(this.volume);
			}
			catch(Exception e)
			{
				//System.out.println("Couldn't set volume");
			}
			
			clip.start();


		}
		catch (Exception e)
		{
			//e.printStackTrace();
		}

	}

	public void loop()
	{
		try
		{
			File f = new File(url);

			URL path = f.toURI().toURL();

			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(path);

			clip = AudioSystem.getClip();

			clip.open(audioInputStream);
			
			try
			{
				FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
				volume.setValue(this.volume);
			}
			catch(Exception e)
			{
				System.out.println("Couldn't set volume");
			}
			clip.loop(-1);

		}
		catch (Exception e)
		{
			//e.printStackTrace();
		}

	}


	public void stop()
	{
		if(clip.isRunning())
			clip.close();
	}

	public void setVolume(float newVolume)
	{
		try
		{
			this.volume = -70 * (1 - newVolume);

			this.volume = Math.max(this.volume, -80);
			this.volume = Math.min(this.volume, 6);

			if(clip == null || !clip.isOpen())
				return;

			FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
			volume.setValue(this.volume);
		}
		catch(Exception e)
		{
			System.out.println("Couldn't set volume");
		}
	}



}
