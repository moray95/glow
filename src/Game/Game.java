package Game;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RadialGradientPaint;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import Effects.ImageFactory;
import Game.Elements.Ball;
import Game.Elements.Element;
import Game.Elements.Paddle;
import Game.PowerUP.PowerUP;
import Lighting.Lighting;
import Menus.MenuHandler;

public class Game extends Canvas implements MouseMotionListener
{
	private static final long serialVersionUID = 1L;

	public int width, height;

	protected BufferStrategy strategy;

	protected boolean running;

	protected int fps = 60;

	protected int mouseX, mouseY;

	// protected JFrame frame = new JFrame("Mega desert bricks");

	protected Lighting lighting;

	protected static float gradSize;

	protected static float[] gradFrac;

	protected Color[] gradColors;

	public ArrayList<Element> shapes;
	
	public Score score;

	public Ball ball;
	public Paddle paddle;
	
	public boolean start = false;
	public boolean paused = false;
	public Boolean stopped = false;
	protected boolean rendering = false;
	protected boolean levelWon = false;
	protected boolean lost = false;
	public int life = 4;
	protected int lostFrames = 120;
	protected boolean lostAnimation = false;
	
	protected boolean ballAnimation = true;
	protected int ballAnimOffset = 0;
	// public final Canvas canvas = new Canvas();
	protected Image bgMask;
	
	MenuHandler handle;
	public int level = 0;
	ArrayList<PowerUP> powerUPs = new ArrayList<PowerUP>();
	
	public MultiplayerGame multiplayerGame;

	
	public Game(MenuHandler handle)
	{
		super();
		this.handle = handle;

		// System.setProperty("sun.java2d.opengl", "True");

		// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		width = screenSize.width;
		height = screenSize.height;
		
		setIgnoreRepaint(true);
		setPreferredSize(new Dimension(width, height));
		setBounds(0, 0, width, height);
		setBackground(Color.black);
		addMouseMotionListener(this);
		addMouseListener(new MouseManager());
		addKeyListener(new KeyManager());

		setVisible(true);

		handle.frame.add(this, BorderLayout.CENTER);

		handle.frame.pack();

		handle.frame.setLocationRelativeTo(null);

		handle.frame.setVisible(true);
		
		handle.frame.requestFocus();

		// addMouseMotionListener(this);
		// handle.frame.addMouseMotionListener(this);
		// handle.frame.addKeyListener(new KeyManager());
		// add(canvas, BorderLayout.WEST);
		
		shapes = new ArrayList<Element>();
		
		score = new Score(60);
		
		gradSize = 200f;
		gradFrac = new float[]
		{ 0f, 1f };
		gradColors = new Color[]
		{ Color.black, new Color(0f, 0f, 0f, 0f) };
		
		ball = new Ball(20, width, height);
		
		bgMask = ImageFactory.bgMask.getScaledInstance((int)gradSize*2, (int)gradSize*2, Image.SCALE_SMOOTH);
		ball.setPosition(200, height);
		paddle = new Paddle(200, 20);
		paddle.setPosition(100, height - 200);
		ball.game = this;
	}

	public void stop()
	{
		running = false;
	}

	public void run()
	{

		init();

		lighting = new Lighting(200f);

		createBufferStrategy(3);

		strategy = getBufferStrategy();
		
		int frames = 0;
		long lastTime, lastSec;
		lastTime = lastSec = System.nanoTime();
		running = true;

		while (running && !stopped)
		{
			if(paused)
				continue;
			
			long deltaTime = System.nanoTime() - lastTime;
			lastTime += deltaTime;

			update(deltaTime / 1e9);

			do
			{
				do
				{
					Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
					g.setColor(new Color(255, 255, 255, 0));
					g.clearRect(0, 0, width*2, height*2);
					//g.scale(1.2, 1.2);
					
					//g.setClip((int)(mouseX/2-(gradSize+ball.radius)), (int)(mouseY/2-(gradSize+ball.radius)), (int)gradSize/2, (int)gradSize/2);
					g.setClip((int)(ball.shape.getBounds().getMinX()-gradSize), (int)(ball.shape.getBounds().getMaxY()-gradSize), (int)gradSize*2, (int)gradSize*2);
					//g.setClip(0, 0, width, height);
					
					//g.setComposite(new Compos);;
					
					g.setRenderingHint(RenderingHints.KEY_RENDERING,
							RenderingHints.VALUE_RENDER_SPEED);
					g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
							RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
					g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,
							RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
					g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
							RenderingHints.VALUE_ANTIALIAS_ON);
					
					
					render(g);
					
					if(paused)
					{
						handle.pauseMenu.game = handle.getGame();
						handle.pauseMenu.gameThread = Thread.currentThread();
						handle.show(MenuHandler.pauseMenuId);

						try
						{
							synchronized(this)
							{
								wait();
							}
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					else if (stopped)
					{
						stopped = false;
						return;
					}
					
					g.dispose();
				} while (strategy.contentsRestored());
				strategy.show();
			} while (strategy.contentsLost());

			frames++;
			if (System.nanoTime() - lastSec >= 1e9)
			{
				fps = frames;
				frames = 0;
				lastSec += 1e9;
			}

			do
			{
				Thread.yield();
			} while (System.nanoTime() - lastTime < 1e9 / 60);
		}
	}

	protected void init()
	{
		
		shapes = LevelLoader.getLevel(level);
		
		paddle.addPaddleToShapes(shapes);
		/*new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ArrayList<Element> lol = new ArrayList<Element>();
				for(int i = shapes.size() - paddle.paddleNb; i < shapes.size(); i++)
					lol.add(shapes.get(i));
				shapes = lol;
			}
		}).start();*/
	}

	protected void update(double deltaTime)
	{
		/*if (level == 0)
			for (int i = 0; i < shapes.size() - paddle.paddleNb - 1; i++)
				shapes.remove(i);*/

		if (!levelWon)
		{
			paddle.setPositionRelativeToX(mouseX-(int)((paddle.shape.getBounds().getWidth()*paddle.paddleNb)/2) <= 0 ? 0 : mouseX-(int)((paddle.shape.getBounds().getWidth()*paddle.paddleNb)/2), width, shapes);
			if (start == true)
			{
				ballAnimOffset = (int)(height);
				ball.update(paddle, shapes);
				if (ball.shape.getBounds().getMaxY() >= height)
					lost = true;
			}
			else
			{
				paddle.shape = shapes.get(shapes.size()-paddle.paddleNb).shape;
				if (ballAnimation == true && ballAnimOffset >= -height+paddle.shape.getBounds().getMinY())
				{
					ball.setPosition((int)(shapes.get(shapes.size()-paddle.paddleNb).shape.getBounds().getX()-paddle.paddleNb*ball.radius/2 + ball.shape.getBounds2D().getWidth()/2), (int)(height-ballAnimOffset));
					ballAnimOffset -= (int)(ball.velocity.getY()/1.5);
				}
				else
					ball.setPosition((int)(shapes.get(shapes.size()-paddle.paddleNb).shape.getBounds().getX()-paddle.paddleNb*ball.radius/2 + ball.shape.getBounds2D().getWidth()/2), (int)(paddle.shape.getBounds().getMinY()-paddle.shape.getBounds().height));
					
				if (ball.shape.getBounds().getMaxY() == height-(height-paddle.shape.getBounds().getMinY()))
					ballAnimation = false;
				if (ball.shape.getBounds().getMaxY() == height-paddle.shape.getBounds().getMinY() && lostAnimation == true)
				{
					lostAnimation = false;
					ballAnimation = false;
					start = false;
				}
			}
		
			ArrayList<PowerUP> newPowerUps = new ArrayList<PowerUP>();
		
			for(PowerUP p : powerUPs)
				if(p.update(this, (float) deltaTime) == -1)
					newPowerUps.add(p);
		
			powerUPs = newPowerUps;
		
			if(ball.collisionWithBrick)
			{
				PowerUP p = PowerUP.randomPowerUP(7, (int) ball.collisionWithBrickPoint.getX(), (int) ball.collisionWithBrickPoint.getY());
				if(p != null)
					powerUPs.add(p);
				
				if (multiplayerGame != null)
					multiplayerGame.brockenBricks.add(ball.collisionWithBrickIndex);
				
			}
			if (shapes.size() == paddle.paddleNb)
				levelWon = true;
		}
	}

	protected void render(Graphics2D g)
	{
		rendering = true;
		if (!levelWon)
		{
			g.setColor(Color.white);
			//g.drawString("FPS: " + fps, (float)(ball.shape.getBounds().getMinX()-gradSize), (float)(ball.shape.getBounds().getMinY()-gradSize));

		
			g.drawImage(ImageFactory.levelImage, 0, 0, null);
		
			lighting.renderShadows(g, shapes, ball.getX(), ball.getY());

			for (int i = 0; i < shapes.size() - paddle.paddleNb; i++)
			{
				if (shapes.get(i).gradient)
				{
					gradColors[0] = shapes.get(i).color;
					g.setPaint(new RadialGradientPaint(new Point2D.Float(ball.getX(),
						ball.getY()), gradSize, gradFrac, gradColors));
				}	
				else
					g.setColor(shapes.get(i).color);
			
				g.fill(shapes.get(i).shape);
			}
			paddle.Draw(g, shapes.get(shapes.size() - paddle.paddleNb));
		
			ball.Draw(g, null);
			
			if (multiplayerGame != null)
				multiplayerGame.render(g);
		
			score.Draw(g, ball);
			
			for(PowerUP p : powerUPs)
				p.draw(g);
			
			g.drawImage(ImageFactory.bgMask, (int)(ball.shape.getBounds().getMinX()-gradSize), (int)(ball.shape.getBounds().getMaxY()-gradSize), null);
			
			if (lost && life > 0 )
			{
				life--;
				lost = false;
				start = false;
				ballAnimation = true;
				lostFrames = 119;
				ball.velocity.setLocation(0, 7);
			}
			if (lostFrames < 120 && lostFrames > 0)
			{
				lostFrames--;
				g.setColor(Color.red);
				g.setFont(new Font(g.getFont().getName(), g.getFont().getStyle(), 25));
				g.drawString("Remaining life : " + life, ball.getX() - 50, ball.getY() - 50);
				start = false;
				ballAnimation = true;
				lostAnimation = true;
			}
			
			if (lostFrames <= 1)
				lostFrames = 120;
			
			if (life == 0)
			{
				start = false;
				stopped = true;
				handle.saveScoreMenu.setScore(score.score);
				handle.show(MenuHandler.saveScoreMenuId);
			}
		}
		else
		{
			System.out.println("Level complete");
			handle.levelCompleteMenu.setScore(score.score);
			handle.levelCompleteMenu.setLevel(level + 1);
			handle.show(MenuHandler.levelCompleteMenuId);
			synchronized(this)
			{
				try {
					wait();
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
			}
		}
		
		rendering = false;
	}

	public void startNextLevel()
	{
		level++;
		System.out.println(level % 5);
		shapes = LevelLoader.getLevel(level);
		paddle.addPaddleToShapes(shapes);
		
		score.scoreColors.clear();
		score.scoreFrames.clear();
		score.scores.clear();
		
		powerUPs.clear();
		
		
		levelWon = false;
		ballAnimation = true;
		start = false;
	}
	
	
	@Override
	public void mouseDragged(MouseEvent e)
	{
		mouseX = e.getX();
		mouseY = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		mouseX = e.getX();
		mouseY = e.getY();
	}

	private class MouseManager implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e) {
			start = true;
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {	
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			
		}
		
	}
	
	private class KeyManager implements KeyListener
	{

		@Override
		public void keyPressed(KeyEvent e)
		{
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
			{
				paused = true;
			}
		}

		@Override
		public void keyReleased(KeyEvent e)
		{

		}

		@Override
		public void keyTyped(KeyEvent e)
		{

		}

	}
}