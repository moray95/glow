package Game.Elements;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;

public class Element
{
	public Shape shape;

	public Color color;

	public Boolean gradient;

	public Dimension size = new Dimension();

	public void setBounds(Integer x, Integer y, Integer width, Integer height)
	{
		if (this.getClass() == Paddle.class || this.getClass() == Brick.class)
			((Rectangle2D)shape).setRect(x, y, width, height);
	}

	public void Draw(Graphics2D g, GradientPaint paint)
	{
		if (paint != null)
			g.setPaint(paint);
		g.setColor(color);
		g.fill(shape);
	}
}