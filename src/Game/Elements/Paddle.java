package Game.Elements;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.List;

import javax.imageio.ImageIO;

public class Paddle extends Element
{
	public int speed;
	public int paddleNb = 10;
	private static Image image;
	
	static
	{
		try
		{
			image = ImageIO.read(new File("res/images/pad.png"));
		}
		catch(Exception e)
		{
			System.out.println("Couldn't load paddle image");
		}
	}
	
	public Paddle(int width, int height)
	{
		this.shape = new Rectangle2D.Float(0, 0, width, height);
		this.speed = 1;
		this.color = Color.LIGHT_GRAY;
		this.gradient = false;
			
	}
	
	public void addPaddleToShapes(List<Element> shapes)
	{
		for (int i = 0; i < paddleNb; i++)
		{
			shapes.add(new Paddle((int)(this.shape.getBounds2D().getX()+i*this.shape.getBounds2D().getY()), (int)(this.shape.getBounds2D().getY())));
			shapes.get(shapes.size()-1).setBounds((int)this.shape.getBounds2D().getX()+i*20, (int)this.shape.getBounds2D().getY(), 0, (int) this.shape.getBounds2D().getHeight());
		}
	}

	public void setPositionRelativeToX(int x, int screenW, List<Element> shapes)
	{
		for (int i = 0; i < paddleNb; i++)
			((Rectangle2D)shapes.get(shapes.size()-i-1).shape).setRect(x >= 0 && x <= screenW - shape.getBounds().getWidth()*paddleNb ? x + i*20 : screenW - paddleNb*20 + i*20, shape.getBounds().getY(), 20, shape.getBounds().getHeight());
	}

	public void setPosition(int x, int y)
	{
		((Rectangle2D) shape).setRect(x, y, shape.getBounds().getWidth(), shape
				.getBounds().getHeight());
	}
	
	
	public void Draw(Graphics2D g, Element e)
	{
		Image im = image.getScaledInstance((int) e.shape.getBounds().getWidth() * paddleNb, (int) e.shape.getBounds().getHeight(), Image.SCALE_SMOOTH);
		AffineTransform a = new AffineTransform();
		
		a.translate(e.shape.getBounds().x - (paddleNb - 1)*20, e.shape.getBounds().y);
		g.drawImage(im, a, null);
	}
}
