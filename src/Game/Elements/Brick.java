package Game.Elements;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.util.Random;

public class Brick extends Element
{
	public Brick(int x, int y, int width, int height, Color color, Boolean gradient)
	{
		if (color == null)
			this.color = new Color(new Random().nextInt(256),
					new Random().nextInt(256), new Random().nextInt(256));
		else
			this.color = color;
		shape = new Rectangle2D.Float(x, y, width, height);
		this.size.setSize(width, height);
		this.gradient = gradient;
	}
	
	public void setPosition(int x, int y)
	{
		((Rectangle2D)shape).setRect(x, y, shape.getBounds().getWidth(), shape.getBounds().getHeight());
	}
}