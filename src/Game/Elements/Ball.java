package Game.Elements;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import Effects.Sound;
import Game.Collider;
import Game.Collider.CollisionResponse;
import Game.Collider.CollisionSide;
import Game.Game;

public class Ball extends Element
{
	public int radius;
	public Point2D velocity;
	public Collider collider;
	public long angle;
	public float speedMultipliyer = 1f;
	public Boolean collisionWithBrick = false;
	public Boolean collisionWithPaddle = false;
	public Point2D collisionWithBrickPoint;
	public Point2D collisionWithPaddlePoint;
	public int collisionWithBrickIndex = 0;
	public boolean once = false;
	public Game game;
	
	Date lastCollision = new Date();
	
	public Ball(int radius, int screenWidth, int screenHeight)
	{
		this.angle = 90;
		this.shape = new Ellipse2D.Float(0, 0, radius, radius);
		this.radius = radius;
		this.color = Color.white;
		this.velocity = new Point2D.Float(1, -7);
		this.collider = new Collider(screenWidth, screenHeight);
		this.collisionWithBrickPoint = new Point2D.Float();
		this.collisionWithPaddlePoint = new Point2D.Float();
	}
	
	public void update(Paddle paddle, ArrayList<Element> shapes)
	{
		Boolean collision = false;
		collisionWithBrick = false;
		collisionWithPaddle = false;
		CollisionResponse r = null;
		
		//this.velocity.setLocation(speedMultipliyer*7*Math.cos(this.angle), speedMultipliyer*7*Math.sin(angle));
		if (getX() <= 0 && getY() <= 0)
			velocity.setLocation(-velocity.getX(), -velocity.getY());
		
		for (int i = 0; i < shapes.size() - paddle.paddleNb; i++)
		{
			r = this.collider.collides(this, shapes.get(i));
			if (r.side != Collider.CollisionSide.None && !r.isBorder)
			{
				once = true;
				collisionWithBrick = true;
				collisionWithBrickPoint = new Point2D.Float(getX(), getY());
				collision = true;
				collisionWithBrickIndex = i;
				shapes.remove(i);
				break;
			}
			else if (r.side != Collider.CollisionSide.None && r.isBorder)
			{
				once = true;
				collision = true;
				break;
			}
		}
		
		if (collision == false)
		{
			for (int i = shapes.size()-paddle.paddleNb; i < shapes.size(); i++)
			{
				r = this.collider.collides(this, shapes.get(i));
				if (r.side != Collider.CollisionSide.None && !r.isBorder)
				{
					collisionWithPaddlePoint = new Point2D.Float(getX(), getY());
					collisionWithPaddle = true;
					collision = true;
					break;
				}
				else if (r.side != Collider.CollisionSide.None && r.isBorder)
				{
					collision = true;
					break;
				}
			}

		}
		
		if (collision == true)
		{
			if ((r.side != Collider.CollisionSide.None &&  collisionWithBrick) || r.isBorder == true)
			{
				if (r.side == CollisionSide.North)
					velocity.setLocation(velocity.getX(), -velocity.getY());
				else if (r.side == CollisionSide.South)
					velocity.setLocation(velocity.getX(), -velocity.getY());
				else if (r.side == CollisionSide.West)
					velocity.setLocation(-velocity.getX(), velocity.getY());
				else if (r.side == CollisionSide.East)
					velocity.setLocation(-velocity.getX(), velocity.getY());
				
				/*
				if (r.side ==  CollisionSide.North)
					this.angle += velocity.getX() < 0 ? this.angle != 0 ? this.angle : 30 : this.angle != 0 ? -this.angle : 30;
				else if (r.side == CollisionSide.South)
					this.angle += velocity.getX() < 0 ? -this.angle != 0 ? this.angle : 30 : this.angle != 0 ? this.angle : 30;
				else if (r.side == CollisionSide.East)
					this.angle += velocity.getX() < 0 ? -this.angle != 0 ? this.angle : 30 : this.angle != 0 ? this.angle : 30;
				else if (r.side == CollisionSide.West)
					this.angle += velocity.getX() < 0 ? this.angle != 0 ? this.angle : 30 : this.angle != 0 ? -this.angle : 30;
					*/
				//this.angle += 30;
				//System.out.println(this.angle);
				
				if (velocity.getX() == 0)
					velocity.setLocation(new Random().nextInt(7),
							velocity.getY());
				else if (velocity.getY() == 0)
					velocity.setLocation(velocity.getX(),
							new Random().nextInt(7));
			}
			else if (r.side != Collider.CollisionSide.None && collisionWithPaddle && game.start && once)
			{
				double factor = collisionWithPaddlePoint.getX()-paddle.shape.getBounds().getCenterX();
			
				if (factor < -(90*paddle.paddleNb)/10)
				{
					if (r.side == CollisionSide.North)
						velocity.setLocation(velocity.getX() > 0 ? -velocity.getX() : velocity.getX(), -velocity.getY());
					else if (r.side == CollisionSide.West)
						velocity.setLocation(-velocity.getX(), velocity.getY());
					else if (r.side == CollisionSide.East)
						velocity.setLocation(-velocity.getX(), velocity.getY());
					else if (r.side == CollisionSide.South)
						velocity.setLocation(velocity.getX() > 0 ? -velocity.getX() : velocity.getX(), -velocity.getY());
				}
				else
				{
					if (r.side == CollisionSide.North)
						velocity.setLocation(velocity.getX() < 0 ? -velocity.getX() : velocity.getX(), -velocity.getY());
					else if (r.side == CollisionSide.West)
						velocity.setLocation(-velocity.getX(), velocity.getY());
					else if (r.side == CollisionSide.East)
						velocity.setLocation(-velocity.getX(), velocity.getY());
					else if (r.side == CollisionSide.South)
						velocity.setLocation(velocity.getX() < 0 ? -velocity.getX() : velocity.getX(), -velocity.getY());
				}
				if (velocity.getX() == 0 || factor == 0)
					velocity.setLocation(new Random().nextInt(7),velocity.getY());
				else if (velocity.getY() == 0)
					velocity.setLocation(velocity.getX(),new Random().nextInt(7));
				
				/*if (getY() >= paddle.shape.getBounds().getMinY() && getX() >= paddle.shape.getBounds().getMinX() || getY() >= paddle.shape.getBounds().getMinY() && getX() <= paddle.shape.getBounds().getMaxX())
					velocity.setLocation(-velocity.getX(), -velocity.getY());
				*/
			}
			

		//	this.angle += 90;
		}
		
		//this.velocity.setLocation(7*Math.cos(this.angle), 7*Math.sin(angle));
		this.setPosition((int) ((shape.getBounds().getX()+velocity.getX()*speedMultipliyer)), (int) (shape.getBounds().getY()+velocity.getY()*speedMultipliyer));	
	}
	
	public void setSpeedMultipliyer(float speedMultipliyer)
	{
		this.speedMultipliyer = speedMultipliyer;
		this.velocity.setLocation(this.velocity.getX()*speedMultipliyer, this.velocity.getY()*speedMultipliyer);
	}
	
	public void setPosition(int x, int y)
	{
		this.shape = new Ellipse2D.Float(x, y, radius, radius);
	}
	
	public int getX()
	{
		return (int)shape.getBounds2D().getCenterX();
	}
	
	public int getY()
	{
		return (int)shape.getBounds2D().getCenterY();
	}
}