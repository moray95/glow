package Game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import Game.Elements.Ball;

public class Score
{
	ArrayList<Point2D.Float> scores;
	ArrayList<Integer> scoreFrames;
	ArrayList<Color> scoreColors;
	
	public int frameCount;
	public int score = 0;
	
	public Score(int frameCount)
	{
		scores = new ArrayList<Point2D.Float>();
		scoreFrames = new ArrayList<Integer>();
		scoreColors = new ArrayList<Color>();
		
		this.frameCount = frameCount;
	}
	
	public void Draw(Graphics2D g, Ball ball)
	{
		if (ball.collisionWithBrick)
		{
			scores.add(new Point2D.Float((int)ball.collisionWithBrickPoint.getX(), (int)ball.collisionWithBrickPoint.getY()));
			scoreFrames.add(60);
			scoreColors.add(Color.red);
			score += 10;
		}
		
		if (scores.size() > 0)
			for (int i = 0; i < scores.size(); i++)
				if (scoreFrames.get(i) > 0)
				{
					Color color = scoreColors.get(i);
					scoreColors.set(i, new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha() - 4 >= 0 ? color.getAlpha() - 4 : 0));
					g.setColor(scoreColors.get(i));
					g.setFont(new Font(g.getFont().getName(), g.getFont().getStyle(), 25));
					scoreFrames.set(i, scoreFrames.get(i)-1);
					
					Point2D.Float tmp = scores.get(i);
					g.drawString("+10", (int)tmp.getX(), (int)tmp.getY());
					tmp.setLocation(tmp.getX(), tmp.getY()-1);
					scores.set(i, tmp);
				}
				else 
				{
					scoreFrames.remove(i);
					scores.remove(i);
					scoreColors.remove(i);
				}
	}
	
	public void addScore(int x, int y)
	{
		scores.add(new Point2D.Float(x, y));
		scoreColors.add(Color.RED);
		scoreFrames.add(frameCount);
		score += 10;
	}
}
