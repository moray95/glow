package Game;

import java.awt.Graphics2D;
import java.util.ArrayList;

import Game.Elements.Ball;
import Game.Elements.Brick;
import Game.Elements.Element;
import Game.Elements.Paddle;
import Networking.Client;
import Networking.ClientDelegate;
import Networking.Server;
import Networking.ServerDelegate;

public class MultiplayerGame implements ServerDelegate, ClientDelegate
{
	public Client client;
	public Server server;
	
	public Game game;
	Ball ball;
	Paddle paddle;
	public ArrayList<Element> opponentShapes = new ArrayList<Element>();
	public ArrayList<Integer> brockenBricks = new ArrayList<Integer>();
	
	Thread networkThread;
	boolean test = false;
	
	public MultiplayerGame(Game game)
	{
		this.game = game;
		ball = new Ball(20, game.width, game.height);
		paddle = new Paddle(200, 20);
		paddle.setPosition(100, 200);
		opponentShapes.add(new Brick(0,0,0,0, null, null));
		paddle.addPaddleToShapes(opponentShapes);
		opponentShapes = new ArrayList<Element>();
	}
	
	
	public void start()
	{
		networkThread = new Thread(new Runnable()
		{
			
			@Override
			public void run()
			{
				while(true)
				{
					sendInfo();
					try
					{
						Thread.sleep(20);
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
		});
		networkThread.start();
	}
	
	void setClient(Client c)
	{
		client = c;
		client.delegate = this;
	}
	
	void setServer(Server s)
	{
		server = s;
		server.delegate = this;
	}
	
	
	
	public void render(Graphics2D g)
	{
		
		ball.setPosition(ball.getX(), game.height - ball.getY());
		ball.Draw(g, null);
		
		if (!test)
		{
			test = true;
			opponentShapes.add(new Brick(0,0,0,0, null, null));
			paddle.addPaddleToShapes(opponentShapes);
		}
		paddle.setPositionRelativeToX(100, game.width, opponentShapes);
		paddle.Draw(g, opponentShapes.get(1));
		
		game.lighting.renderShadows(g, opponentShapes, ball.getX(), ball.getY());
		game.lighting.renderShadows(g, game.shapes, ball.getX(), ball.getY());
		game.lighting.renderShadows(g, opponentShapes, game.ball.getX(), game.ball.getY());
	}
	
	void sendInfo()
	{
		String msg = "";
		
		// Paddle info
		try
		{
			msg += ((double) game.shapes.get(game.shapes.size() - game.paddle.paddleNb).shape.getBounds().x) / game.width + " ";
			msg += game.paddle.paddleNb + "\n";

			// Ball info
			msg += game.ball.shape.getBounds().getX() + " " + game.ball.shape.getBounds().getY() + "\n";

			for(int i = 0; i < brockenBricks.size(); i++)
			{
				msg += brockenBricks.get(i) + " ";
			}
			brockenBricks.clear();
			msg = msg.substring(0, msg.length() - 2);
			client.sendMessage(msg);
		}
		catch(Exception e)
		{
		}
	}
	
	@Override
	public void didSendMessage(String msg)
	{
	}

	@Override
	public void failedSendingMessage(String msg)
	{
		System.out.println("Failed sending message: " + msg);
	}

	@Override
	public void messageReceived(String msg)
	{
		try
		{
			String[] lines = msg.split("\n");
			String[] paddle = lines[0].split("\\ ");
			double paddleX = Double.parseDouble(paddle[0]) * game.width;
			int paddleNb = Integer.parseInt(paddle[1]);
			
			String[] ball = lines[1].split("\\ ");
			double ballX = Double.parseDouble(ball[0]);
			double ballY = Double.parseDouble(ball[1]);
			
			ArrayList<Integer> removedBricks = new ArrayList<Integer>();
			
			for(int i = 2; i < lines.length; i++)
				removedBricks.add(Integer.parseInt(lines[i]));
			
			this.ball.setPosition((int) ballX, (int) ballY);
			this.paddle.setPositionRelativeToX((int) paddleX, game.width, this.opponentShapes);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Received corrupted message: " + msg);
		}
		
	}
	
	public void removeShape(int i)
	{
		game.shapes.remove(i);
	}
}
