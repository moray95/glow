package Game;

import java.awt.Rectangle;
import java.awt.Shape;

import Game.Elements.Element;


public class Collider
{
	public enum CollisionSide
	{
		North,
		East,
		West,
		South,
		None
	}

	public static class CollisionResponse
	{
		public CollisionSide side;
		public boolean isBorder;
	}

	Shape shape;
	int width;
	int height;

	public Collider(int width, int height)
	{
		this.width = width;
		this.height = height;
	}


	public CollisionResponse collides(Element e1, Element e)
	{
		Shape s1 = e1.shape;
		Shape s2 = e.shape;

		Rectangle rect = s1.getBounds();

		CollisionResponse response = new CollisionResponse();

		if(rect.getMaxX() >= width)
		{
			response.isBorder = true;
			response.side = CollisionSide.West;
			return response;
		}
		if(rect.x <= 0)
		{
			response.isBorder = true;
			response.side = CollisionSide.East;
			return response;
		}
		if(rect.y <= 0)
		{
			response.isBorder = true;
			response.side = CollisionSide.South;
			return response;
		}
		if(rect.getMaxY() >= height)
		{	
			response.isBorder = true;
			response.side = CollisionSide.North;
			return response;
		}

		double centerX = s2.getBounds().getCenterX();
		double centerY = s2.getBounds().getCenterY();		


		response.isBorder = false;


		if(centerX >= rect.x && centerX <= rect.getMaxX() && s2.getBounds().getMinY() <= rect.getMaxY() && centerY >= rect.getMaxY())
		{
			response.side = CollisionSide.South;
			return response;
		}

		if(centerX >= rect.x && centerX <= rect.getMaxX() && s2.getBounds().getMaxY() >= rect.getMinY() && centerY <= rect.getMinY())
		{
			response.side = CollisionSide.North;
			return response;
		}
		if(centerX <= rect.x && centerY <= rect.getMaxY() && centerY >= rect.getMinY() && s2.getBounds().getMaxX() >= rect.x)
		{
			response.side = CollisionSide.East;
			return response;
		}
		if(s2.getBounds().x <= rect.getMaxX() && centerY <= rect.getMaxY() && centerY >= rect.getMinY() && s2.getBounds().getMaxX() >= rect.x)
		{
			response.side = CollisionSide.West;
			return response;
		}
		response.side = CollisionSide.None;
		return response;
	}
}
