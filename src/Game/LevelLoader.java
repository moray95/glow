package Game;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import Game.Elements.Brick;
import Game.Elements.Element;

public class LevelLoader
{
	public static ArrayList<Element> getLevel(int i)
	{
		i = i % 5 + 1;
		ArrayList<Element> elements = new ArrayList<Element>();
		
		File f = new File("res/levels/level" + i);
		try
		{
			Scanner s = new Scanner(f, "UTF-8");
			while(s.hasNextLine())
			{
				String line = s.nextLine();
				if(line.equals(""))
					continue;
				
				String[] args = line.split(" ");
				int x = Integer.parseInt(args[0]);
				int y = Integer.parseInt(args[1]);
				int m = args.length > 2 ? Integer.parseInt(args[2]) : 1;
				for(int j = 0; j < m; j++)
					elements.add(new Brick(x + j*20, y, 20, 20, null, true));
			}
			s.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return elements;
	}
}
