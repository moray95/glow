package Game.PowerUP;

import Effects.ImageFactory;
import Game.Game;

public class FastenPowerUP extends PowerUP
{

	public FastenPowerUP(int x, int y)
	{
		super(x, y);
		image = ImageFactory.fastenPowerUp;
	}
	
	@Override
	public int run(Game game)
	{
		if(game.ball.speedMultipliyer >= 5)
			return 0;
		
		
		game.ball.setSpeedMultipliyer(game.ball.speedMultipliyer*1.2f);
		return 0;
	}

}
