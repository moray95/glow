package Game.PowerUP;

import Effects.ImageFactory;
import Game.Game;

public class LifePowerUP extends PowerUP
{
	
	public LifePowerUP(int x, int y)
	{
		super(x, y);
		image = ImageFactory.extraLifePowerUp;
	}

	@Override
	public int run(Game game)
	{
		game.life++;
		
		return 0;
	}
	
	

}
