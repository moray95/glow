package Game.PowerUP;

import java.awt.Point;
import java.util.ArrayList;

import Effects.Sound;
import Game.Game;
import Game.Elements.Ball;
import Game.Elements.Element;
import Game.Elements.Paddle;

public class ExplosionPowerUP extends PowerUP
{
	int range = 200;
	
	public ExplosionPowerUP(int x, int y)
	{
		super(x, y);
		image = null;
	}
	
	@Override
	public int run(Game game)
	{
		ArrayList<Element> elements= game.shapes;
		Paddle paddle = game.paddle;
		Ball ball = game.ball;
		
		int clearBricks = 0;
		Point location = new Point( (int) ball.shape.getBounds().getCenterX(), (int) ball.shape.getBounds().getCenterY());
		ArrayList<Element> newElements = new ArrayList<Element>();
		
		Sound.explosion.play();
		
		for(int i = 0; i < elements.size() - paddle.paddleNb; i++)
		{
			Element e = elements.get(i);
			Point p = new Point( (int) e.shape.getBounds().getCenterX(), (int) e.shape.getBounds().getCenterY());
			if(p.distance(location) <= range)
			{
				game.score.addScore(p.x, p.y);
				game.shapes.remove(i);
				clearBricks++;
			}
			else
			{
				newElements.add(elements.get(i));
			}
		}
		for(int i = elements.size() - paddle.paddleNb; i < elements.size(); i++)
			newElements.add(elements.get(i));
		
		game.shapes = newElements;
		return clearBricks;
	}
	
	@Override
	public int update(Game game, float dt)
	{
		return run(game);
		
	}
}
