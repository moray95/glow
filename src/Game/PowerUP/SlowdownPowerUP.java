package Game.PowerUP;

import Effects.ImageFactory;
import Game.Game;

public class SlowdownPowerUP extends PowerUP
{
	
	public SlowdownPowerUP(int x, int y)
	{
		super(x, y);
		image = ImageFactory.slowdownPowerUp;
	}
	
	@Override
	public int run(Game game)
	{
		if(game.ball.speedMultipliyer < 0.5)
			return 0;
		
		
		game.ball.setSpeedMultipliyer(game.ball.speedMultipliyer / 1.2f);
		return 0;
	}

}
