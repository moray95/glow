package Game.PowerUP;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.geom.Point2D;
import java.util.Random;

import Game.Collider;
import Game.Game;
import Game.Elements.Paddle;

public abstract class PowerUP
{
	int speed = 20;
	Image image;
	Point2D.Float position;
	
	public PowerUP(int x, int y)
	{
		position = new Point2D.Float(x, y);
	}
	
	public void draw(Graphics2D g)
	{
		g.drawImage(image, (int) position.x, (int) position.y, null);
	}
	
	public int update(Game game, float dt)
	{
		Paddle paddle = game.paddle;
		
		position.y += speed * dt*5;
		if(position.y > Toolkit.getDefaultToolkit().getScreenSize().getHeight())
			return 0;
		
		Collider collider = new Collider(0, 0);
		
		Rectangle rect = new Rectangle((int) position.x, (int) position.y, image.getWidth(null), image.getHeight(null));
		
		for (int i = game.shapes.size() - paddle.paddleNb; i < game.shapes.size(); i++)
		{
			if (rect.intersects(game.shapes.get(i).shape.getBounds()))
			{
				run(game);
				return 0;
			}
		}
		
		return -1;
	}
	
	public abstract int run(Game game);
	
	public static PowerUP randomPowerUP(int chance, int x, int y)
	{
		Random rand = new Random(new Random().nextLong());
		int r = rand.nextInt(101);
		if(r < (100 - chance))
			return null;
		r = rand.nextInt(8);
		
		switch(r)
		{
		case 0:
			return new EnlargePowerUP(x, y);
		case 1:
			return new FastenPowerUP(x, y);
		case 2:
			return new ShrinkPowerUP(x, y);
		case 3:
			return new SlowdownPowerUP(x, y);
		case 4:
			return new LifePowerUP(x, y);
		default:
			return new ExplosionPowerUP(x, y);
		}
		
	}
	
}