package Game.PowerUP;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;

import Effects.ImageFactory;
import Game.Game;
import Game.Elements.Brick;
import Game.Elements.Element;
import Game.Elements.Paddle;

public class EnlargePowerUP extends PowerUP
{
	public EnlargePowerUP(int x, int y)
	{
		super(x, y);
		image = ImageFactory.enlargePowerUp;
	}
	@Override
	public int run(Game game)
	{
		
		Paddle paddle = game.paddle;
		if(paddle.paddleNb >= 20)
			return 0;
		
		ArrayList<Element> elements = game.shapes;
		
		int newSize = (int) (paddle.paddleNb * 1.2);
		
		Rectangle rect = game.shapes.get(game.shapes.size() - paddle.paddleNb).shape.getBounds();
		
		for(int i = paddle.paddleNb; i < newSize; i++)
		{
			Brick e = new Brick(rect.x + 20 * i, rect.y, 20, 20, Color.LIGHT_GRAY, null);
			elements.add(e);
		}
		
		paddle.paddleNb = newSize;
		return 0;
	}

}
