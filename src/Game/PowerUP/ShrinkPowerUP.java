package Game.PowerUP;

import java.util.ArrayList;

import Effects.ImageFactory;
import Game.Game;
import Game.Elements.Element;
import Game.Elements.Paddle;

public class ShrinkPowerUP extends PowerUP
{
	public ShrinkPowerUP(int x, int y)
	{
		super(x, y);
		image = ImageFactory.shrinkPowerUp;
	}

	@Override
	public int run(Game game)
	{
		Paddle paddle = game.paddle;
		if(paddle.paddleNb <= 2)
			return 0;
			
		ArrayList<Element> elements = game.shapes;
		ArrayList<Element> bricks = new ArrayList<Element>();
		
		ArrayList<Element> newElements = new ArrayList<Element>();
		
		int newSize = (int) (paddle.paddleNb / 1.5);
		for(int i = 0; i < elements.size() - paddle.paddleNb; i++)
			bricks.add(elements.get(i));
		
		for(int i = elements.size() - paddle.paddleNb; i < elements.size() - paddle.paddleNb + newSize; i++)
			newElements.add(elements.get(i));

		paddle.paddleNb = newSize;
		bricks.addAll(newElements);
		game.shapes = bricks;
		
		return 0;
		
	}

}
