package Networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Server
{
	ServerSocket sSocket;
	public int port = 8880;
	public ServerDelegate delegate;
	Thread thread;
	
	
	public Server(ServerDelegate delegate)
	{
		super();
		this.delegate = delegate;
		try
		{
			sSocket = new ServerSocket(port);
		}
		catch(IOException e)
		{
			System.out.println("Error creating server: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public Server()
	{
		try
		{
			sSocket = new ServerSocket(port);
		}
		catch(IOException e)
		{
			System.out.println("Error creating server: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void start()
	{
		if(thread != null && thread.isAlive())
			thread.stop();
		
		thread = new Thread(new Runnable()
		{
			
			@Override
			public void run()
			{
				open();
			}
		});
		thread.start();
	}
	
	void open()
	{
		Socket client;
		try
		{
			while((client = sSocket.accept()) != null)
			{
				InputStream input = client.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(input));
				String message = "";
				String line;
				while((line = reader.readLine()) != null)
				{
					message += line + "\n";
				}
				message = message.substring(0,  message.length() - 1);
				
				if (delegate != null)
				{
					delegate.messageReceived(message);
				}
			}
		}
		catch(IOException e)
		{
			System.out.println("Error retreiving message: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void close()
	{
		try
		{
			thread.stop();
			sSocket.close();
		}
		catch (IOException e)
		{
			System.out.println("Error closing server: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
}


