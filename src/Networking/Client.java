package Networking;

import java.io.IOException;
import java.net.Socket;



public class Client
{
	String message;
	public String host;
    public static int port = 8880;
    public ClientDelegate delegate;
    
    public Client()
    {
    }

    public Client(String host, ClientDelegate delegate)
    {
        this.host = host;
        this.delegate = delegate;
    }
   
    
    public void sendMessage(String msg)
    {
    	this.message = msg;
    	new Thread(new Runnable()
    	{
			
			@Override
			public void run()
			{
				Socket s = null;
				
				try
		        {
		            s = new Socket(host, port);
		            s.getOutputStream().write(message.getBytes());
		            s.close();
		            if(delegate != null)
		            {
		            	delegate.didSendMessage(message);
		            }
		        }
		        catch (IOException e)
				{
		        	e.printStackTrace();
		        	System.out.println("Error sending message: " + e.getMessage());
		        	if (delegate != null)
		        	{
		        		delegate.failedSendingMessage(message);
		        	}
				}
				finally
				{
					try
					{
						if (s != null)
							s.close();
					}
					catch (IOException e)
					{
						
						e.printStackTrace();
					}
				}
			}
		}).start();
    }
    
    /*
    public static void sendBroadcast(final ClientDelegate delegate, final String msg)
    {
    	
			
			
    }
    
    
    
    private static String broadcastAddress()
	{
		try
		{
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

			while (interfaces.hasMoreElements())
			{

				NetworkInterface networkInterface = interfaces.nextElement();


				if (networkInterface.isLoopback())
					continue;    // Don't want to broadcast to the loopback interface

				for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses())
				{
					InetAddress broadcast = interfaceAddress.getBroadcast();

					if (broadcast == null)
						continue;

					return broadcast.getHostAddress();

				}

			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}*/

}
