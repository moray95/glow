package Networking;

public interface ClientDelegate
{
	void didSendMessage(String msg);
	void failedSendingMessage(String msg);
}
