package Networking;

public interface ServerDelegate
{
	void messageReceived(String msg);
}