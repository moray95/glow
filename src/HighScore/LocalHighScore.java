package HighScore;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public final class LocalHighScore extends HighScore
{
	private static Preferences prefs = Preferences.userRoot().node(LocalHighScore.class.getName());
	private static final String localHighScores = "MDBHighScores";
	
	public LocalHighScore(String name, long score)
	{
		super(name, score);
	}
	
	public static void addHighScores(HighScore hs)
	{
		ArrayList<HighScore> scores = getHighScores();
		int i;
		for(i = 0; i < scores.size() && scores.get(i).score < hs.score; i++);
		
		scores.add(i, hs);
		setHighScores(scores);
	}
	
	private static void setHighScores(ArrayList<HighScore> list)
	{
		String str = "";
		for (HighScore hs : list)
		{
			str += hs.name + ":" + hs.score + "\n";
		}
		prefs.put(localHighScores, str);
	}
	
	public static ArrayList<HighScore> getHighScores()
	{
		String str = prefs.get(localHighScores, "");
		if (str == "")
			return new ArrayList<HighScore>();
		
		String[] lines = str.split("\n");
		
		ArrayList<String[]> scoresArray = new ArrayList<String[]>();
		ArrayList<HighScore> scores = new ArrayList<HighScore>();
		
		for (int i = 0; i < lines.length; i++)
		{
			String line = lines[i];
			scoresArray.add(line.split(":"));
			scores.add(new HighScore(scoresArray.get(i)[0], Long.parseLong(scoresArray.get(i)[1])));
		}
		
		scores.sort(new Comparator<HighScore>()
		{
			@Override
			public int compare(HighScore o1, HighScore o2)
			{
				return (int) (o2.score - o1.score);
			}
		});
		return scores;
	}
	
	public static void resetHighScores()
	{
		try
		{
			prefs.clear();
		}
		catch (BackingStoreException e)
		{
			System.out.println("Error resetting local high scores: " + e.getMessage());
			e.printStackTrace();
		}
	}

}
