package HighScore;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;

public final class DBHighScore extends HighScore
{

	static final String username = "moray95";
	static final String password = "moraybaruh1995";
	static final String addUrl = "http://www.glow.ed-fame.com/add_score.php";
	static final String getUrl = "http://www.glow.ed-fame.com/get_scores.php";

	public static HighScoreConnectionDelegate delegate;

	static HighScore highScoreToSend = null;

	public DBHighScore(String name, int score)
	{
		super(name, score);
	}

	public static void getHighScores()
	{
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				URL url;
				HttpURLConnection connection = null;

				try
				{
					url = new URL(getUrl);
					connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("POST");
					connection.setUseCaches(false);
					connection.setDoOutput(true);
					connection.setDoInput(true);

					// Send Request
					DataOutputStream writer = new DataOutputStream(connection
							.getOutputStream());
					String body = "username=" + username + "&password="
							+ password;
					writer.writeBytes(body);
					writer.flush();
					writer.close();

					// Get Response
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(connection.getInputStream()));
					String line;
					StringBuffer buffer = new StringBuffer();
					while ((line = reader.readLine()) != null)
					{
						buffer.append(line + "\n");
					}
					reader.close();
					if(delegate != null)
					{
						ArrayList<HighScore> list = HighScore.highScoresFromJSONString(buffer.toString());
						
						list.sort(new Comparator<HighScore>()
						{
							@Override
							public int compare(HighScore o1, HighScore o2)
							{
								return (int) (o2.score - o1.score);
							}
						});
						delegate.didDownloadHighScores(list);
					}

				} catch (Exception e)
				{
					System.out
							.println("Error downloading highscores from database: "
									+ e.getMessage());
					e.printStackTrace();
					if (delegate != null)
					{
						delegate.didDownloadHighScores(null);
					}
				} finally
				{
					if (connection != null)
						connection.disconnect();
				}
			}
		});

		thread.start();
	}

	public static void uploadScoreToDatabase(HighScore hs)
	{
		highScoreToSend = hs;

		Thread thread = new Thread(new Runnable()

		{

			@Override
			public void run()
			{
				HighScore hs = highScoreToSend;
				URL url;
				HttpURLConnection connection = null;

				try
				{
					url = new URL(addUrl);
					connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("POST");
					connection.setUseCaches(false);
					connection.setDoOutput(true);
					connection.setDoInput(true);

					// Send Request
					DataOutputStream writer = new DataOutputStream(connection
							.getOutputStream());
					String body = "username=" + username + "&password="
							+ password + "&name=" + hs.name + "&score="
							+ hs.score;
					writer.writeBytes(body);
					writer.flush();
					writer.close();

					// Get Response
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(connection.getInputStream()));
					String line;
					StringBuffer buffer = new StringBuffer();
					while ((line = reader.readLine()) != null)
					{
						buffer.append(line + "\n");
					}
					reader.close();
					if (delegate != null)
					{
						delegate.didUploadHighScore(hs,
								buffer.toString() == "1 record added1");
					}
				} catch (Exception e)
				{
					System.out
							.println("Error downloading highscores from database: "
									+ e.getMessage());
					e.printStackTrace();
					if (delegate != null)
					{
						delegate.didUploadHighScore(hs, false);
					}
				} finally
				{
					if (connection != null)
						connection.disconnect();
					highScoreToSend = null;
				}

			}
		});

		thread.start();
	}

}
