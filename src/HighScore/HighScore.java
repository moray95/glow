package HighScore;

import java.util.ArrayList;


public class HighScore
{
	public String name;
	public long score;
	
	public HighScore(String name, long score)
	{
		this.name = name;
		this.score = score;
	}
	
	public static ArrayList<HighScore> highScoresFromJSONString(String str)
	{
		ArrayList<HighScore> list = new ArrayList<HighScore>();
		
		ArrayList<Integer> openBraces = new ArrayList<Integer>();
		ArrayList<Integer> closeBraces = new ArrayList<Integer>();
		
		for (int i = 0; i < str.length(); i++)
		{
			switch(str.charAt(i))
			{
			case '{':
				openBraces.add(i);
				break;
			case '}':
				closeBraces.add(i);
				break;
			}
		}
		
		if (closeBraces.size() != openBraces.size())
		{
			return null;
		}
		
		for (int i = 0; i < openBraces.size(); i++)
		{
			list.add(highScoreFromJSONString(str.substring(openBraces.get(i), closeBraces.get(i))));
		}
		
		return list;
	}
	
	
	
	private static HighScore highScoreFromJSONString(String str)
	{
		HighScore hs = new HighScore("", 0);
		ArrayList<Integer> quotes = new ArrayList<Integer>();
		for(Integer i = 0; i < str.length(); i++)
		{
			if(str.charAt(i) == '\"')
			{
				quotes.add(i);
			}
		}
		ArrayList<String> properties = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();
		
		for(int i = 0; i < quotes.size(); i+= 4)
		{
			properties.add(str.substring(quotes.get(i) + 1,quotes.get(i+1)));
			values.add(str.substring(quotes.get(i+2) + 1, quotes.get(i+3)));
		}
		
		for(int i = 0; i < properties.size(); i++)
		{
			switch(properties.get(i))
			{
			case "Name":
				hs.name = values.get(i);
				break;
			case "Score":
				hs.score = Long.parseLong(values.get(i));
				break;
			}
		}
		
		return hs;
	}
	
	@Override
	public String toString()
	{
		return "Name = '" + name + "' score = " + score;
	}
}
