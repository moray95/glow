package HighScore;

import java.util.ArrayList;

public interface HighScoreConnectionDelegate
{
	void didUploadHighScore(HighScore hs, boolean success);
	void didDownloadHighScores(ArrayList<HighScore> highScores);
}
