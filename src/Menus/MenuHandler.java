package Menus;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import Effects.Sound;
import Game.Game;

public class MenuHandler
{

	public static final String mainMenuId = "1";
	public static final String optionsMenuId = "2";
	public static final String newGameId = "3";
	public static final String helpMenuId = "4";
	public static final String highScoresMenuId = "5";
	public static final String pauseMenuId = "6";
	public static final String saveScoreMenuId = "7";
	public static final String levelCompleteMenuId = "8";
	
	CardLayout cl = new CardLayout();

	public JFrame frame = new JFrame("Mega Desert Bricks");
	public JPanel panelCont = new JPanel();

	MainMenu mainMenu = new MainMenu(this);
	OptionsMenu optionsMenu = new OptionsMenu(this);
	HighScoreMenu highScoreMenu = new HighScoreMenu(this);
	public PauseMenu pauseMenu = new PauseMenu(this);
	private Game newGame = new Game(this);
	HelpMenu helpMenu = new HelpMenu(this);
	public SaveScoreMenu saveScoreMenu = new SaveScoreMenu(this);
	public LevelCompleteMenu levelCompleteMenu = new LevelCompleteMenu(this);
	
	public MenuHandler()
	{
		optionsMenu.oldMenu = mainMenuId;
		
		panelCont.setLayout(cl);

		panelCont.add(mainMenu, mainMenuId);
		panelCont.add(optionsMenu, optionsMenuId);
		panelCont.add(newGame, newGameId);
		panelCont.add(highScoreMenu, highScoresMenuId);
		panelCont.add(pauseMenu, pauseMenuId);
		panelCont.add(saveScoreMenu, saveScoreMenuId);
		panelCont.add(levelCompleteMenu, levelCompleteMenuId);
		
		show(mainMenuId);

		panelCont.add(helpMenu, helpMenuId);
	}
	
	
	public void setGame(Game game)
	{
		
		panelCont.remove(newGame);
		newGame = game;
		panelCont.add(newGame, newGameId);
	}
	
	public Game getGame()
	{
		return newGame;
	}
	
	public void show(String id)
	{
		cl.show(panelCont, id);
		frame.repaint();
	}

	public void run()
	{
		Sound.init();

		Sound.music.loop();
		frame.addMouseMotionListener(newGame);

		frame.setResizable(false);
		frame.add(panelCont);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setVisible(true);

		
		GraphicsDevice g = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		
		if(g.isFullScreenSupported())
			g.setFullScreenWindow(frame);
		else
			System.out.println("Full screen not supported");
		
		if(System.getProperty("os.name").equals("Mac OS X"))
			return;
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	
		frame.setBounds(0, 0, screenSize.width, screenSize.height);
		
		frame.validate();
		frame.dispose();
		
		frame.setUndecorated(true);
		if(!System.getProperty("os.name").equals("Linux"))
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		frame.setVisible(true);
		frame.requestFocus();
		frame.toFront();
		frame.repaint();
	}
}