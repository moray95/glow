package Menus.CustomUI;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public abstract class Menu extends JPanel
{
	private static final long serialVersionUID = 1L;
	private static Image image;

	static
	{
		try
		{
			Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
			image = ImageIO.read(new File("res/images/bg.jpg"))
					.getScaledInstance(size.width, size.height,
							Image.SCALE_SMOOTH);

		}
		catch (Exception e)
		{
			System.out.println("Couldn't load background image: "
					+ e.getMessage());
			image = null;
		}
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		((Graphics2D) g).drawImage(image, 0, 0, null);
	}

}
