package Menus.CustomUI;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import Effects.Sound;

public class Button extends JButton implements MouseListener {
	private static final long serialVersionUID = 1L;

	private static Image image;
	private static Image hooverImage;

	static {
		try {
			image = ImageIO.read(new File("res/images/buttons/button.png"))
					.getScaledInstance(100, 50, Image.SCALE_SMOOTH);
			hooverImage = ImageIO.read(
					new File("res/images/buttons/button_hoover.png"))
					.getScaledInstance(100, 50, Image.SCALE_SMOOTH);
		} catch (Exception e) {
			System.out.println("Couldn't load button image: " + e.getMessage());
		}
	}

	public int width = 100;
	public int height = 50;

	public Button(String name) {

		super(name, new ImageIcon(image));
		setBorder(BorderFactory.createEmptyBorder());
		setHorizontalTextPosition(JButton.CENTER);
		setVerticalTextPosition(JButton.CENTER);
		addMouseListener(this);

		setBorderPainted(false);
		setContentAreaFilled(false);
		setFocusPainted(false);
		setOpaque(false);
		setForeground(Color.WHITE);
	}

	@Override
	public void setBounds(int x, int y, int h, int w)
	{
		super.setBounds(x, y, width, height);
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{

	}

	@Override
	public void mousePressed(MouseEvent e)
	{
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		Sound.mouseClick.play();
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				Sound.mouseHoover.play();
			}
		}).start();

		setIcon(new ImageIcon(hooverImage));
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		setIcon(new ImageIcon(image));
	}
}