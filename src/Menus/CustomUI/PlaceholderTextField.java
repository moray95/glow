package Menus.CustomUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import javax.swing.FocusManager;
import javax.swing.JTextField;

public class PlaceholderTextField extends JTextField
{
	private static final long serialVersionUID = 1L;
	String placeholder;

	public PlaceholderTextField(String placeholder)
	{
		super();
		this.placeholder = placeholder;
	}

	@Override
	protected void paintComponent(java.awt.Graphics g)
	{
		super.paintComponent(g);

		if(getText().isEmpty() && ! (FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == this))
		{
			Graphics2D g2 = (Graphics2D)g.create();
			g2.setBackground(Color.gray);
			g2.setFont(getFont().deriveFont(Font.ITALIC));
			g2.drawString(placeholder, 10, 15);
			g2.dispose();
		}
	}
}