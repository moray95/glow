package Menus;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import Effects.ImageFactory;
import Game.Game;
import Game.MultiplayerGame;
import Menus.CustomUI.Button;
import Menus.CustomUI.Menu;
import Menus.CustomUI.PlaceholderTextField;
import Networking.Client;
import Networking.ClientDelegate;
import Networking.Server;
import Networking.ServerDelegate;

public class LanGameMenu extends Menu implements ServerDelegate, ClientDelegate
{		

	private static final long serialVersionUID = 1L;
	private static String startMessage;
	private static String acceptStart = "acceptstart";
	private static String refuseStart = "refusestart";
	//private static String broadcastMessage;
	private static String broadcastResponse;

	Server server;
	Client client;

	String localHost;
	JTable ipTable;

	ArrayList<String> ips = new ArrayList<String>();

	MenuHandler handle;
	private Thread ipThread;
	boolean gameRun = false;
	
	public LanGameMenu(final MenuHandler handle)
	{
		super();
		this.handle = handle;

		setLayout(null);

		try
		{
			NetworkInterface en = NetworkInterface.getNetworkInterfaces().nextElement();

			Enumeration<InetAddress> ee = en.getInetAddresses();
			
			do{
				
				InetAddress ia = (InetAddress) ee.nextElement();
				localHost =	ia.getHostAddress();
				
			}while(localHost.indexOf(':') != -1);
			System.out.println(localHost);
			startMessage = "start " + localHost;
			//broadcastMessage = "broadcast " + localHost;
			broadcastResponse = "broadcastresponse " + localHost;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		server = new Server(this);
		server.start();

		final ClientDelegate delegate = this;

		Button button = new Button("Send");
		button.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					if (ipTable.getSelectedRow() >= 0)
					{
						client = new Client(ips.get(ipTable.getSelectedRow()), delegate);
						client.sendMessage(startMessage);
					}
				}
				catch (Exception e1)
				{
					e1.printStackTrace();
				}
			}
		});

		Button refreshButton = new Button("Refresh");
		refreshButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				ips.clear();
				getIPs();

			}
		});

		ipTable = new JTable(new TableModel()
		{

			@Override
			public void setValueAt(Object aValue, int rowIndex, int columnIndex)
			{
			}

			@Override
			public void removeTableModelListener(TableModelListener l)
			{
			}

			@Override
			public boolean isCellEditable(int rowIndex, int columnIndex)
			{
				return false;
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex)
			{
				if(rowIndex > ips.size())
					return ips.get(rowIndex);
				return null;
			}

			@Override
			public int getRowCount()
			{
				return ips.size();
			}

			@Override
			public String getColumnName(int columnIndex)
			{
				return null;
			}

			@Override
			public int getColumnCount()
			{
				return 1;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex)
			{
				return String.class;
			}

			@Override
			public void addTableModelListener(TableModelListener l)
			{

			}
		});
		JScrollPane scrollPane = new JScrollPane(ipTable);


		Button backButton = new Button("Back");
		backButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				handle.show(MenuHandler.mainMenuId);
			}
		});

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension center = new Dimension(screenSize.width / 2, screenSize.height / 2);

		backButton.setBounds(10, (int) screenSize.getHeight() - 110, 100, 50);
		scrollPane.setBounds( (int) center.getWidth() - 100, 30, 200, 500);
		button.setBounds( (int) center.getWidth() - 100, (int) scrollPane.getBounds().getMaxY() + 10, 100, 50);
		refreshButton.setBounds((int) scrollPane.getBounds().getMaxX() - 100, (int) button.getBounds().getMinY(), 100, 50);


		final PlaceholderTextField ipField = new PlaceholderTextField("Manual IP");
		ipField.setBounds( (int) (scrollPane.getBounds().getMinX()), (int) refreshButton.getBounds().getMaxY() + 10 + 10, 200, 20);

		Button customIPButton = new Button("Send");
		customIPButton.setBounds((int) ipField.getBounds().getCenterX() - 50, (int) ipField.getBounds().getMaxY() + 10, 100, 50);

		customIPButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				String ip = ipField.getText();
				String[] comps = ip.split("\\.");
				try
				{
					if (comps.length != 4)
						throw new Exception("Invalid IP length " + comps.length);

					for(String comp : comps)
					{
						int x = Integer.parseInt(comp);
						if(x > 255 || x < 0)
							throw new Exception("Invalid IP component " + x);
					}
					Client c = new Client(ip, delegate);
					c.sendMessage(startMessage);
				}catch(Exception exception)
				{
					exception.printStackTrace();
					JOptionPane.showMessageDialog(handle.frame.getContentPane(), "The IP you entered is invalid", "Invalid IP", JOptionPane.OK_OPTION, new ImageIcon(ImageFactory.errorImage));

				}
			}
		});





		add(ipField);
		add(customIPButton);




		add(refreshButton);
		add(backButton);
		add(button);
		add(scrollPane);

	}

	@Override
	public void didSendMessage(String msg)
	{
		System.out.println("Faied sending: " + msg);
	}

	@Override
	public void failedSendingMessage(String msg)
	{
		System.out.println("Failed sending: " + msg);
		if(messageType(msg) == MessageType.Start)
		{
			JOptionPane.showMessageDialog(handle.frame.getContentPane(), "Game starting request failed.\nMake sure your host has Mega Desert Bricks running on Lan Game menu.", "Request failed", JOptionPane.OK_OPTION, new ImageIcon(ImageFactory.errorImage));
		}
	}

	void startNewGame()
	{
		Game game = new Game(handle);
		game.multiplayerGame = new MultiplayerGame(game);
		game.multiplayerGame.client = client;
		game.multiplayerGame.client.delegate = game.multiplayerGame;
		server.delegate = game.multiplayerGame;
		game.multiplayerGame.server = server;
		
		handle.setGame(game);
		gameRun = true;
		handle.show(MenuHandler.newGameId);
		
		new Thread(new Runnable()
		{
			
			@Override
			public void run()
			{
				for(int i = 0; i < 100000; i++);
				handle.getGame().multiplayerGame.start();
				handle.getGame().run();
			}
		}).start();
	}
	
	@Override
	public void messageReceived(String msg)
	{
		MessageType msgType = messageType(msg);
		System.out.println("Message received: " + msg);

		switch(msgType)
		{
		case Start:
			String host = msg.split(" ")[1];

			System.out.println("start request from " + host);
			int ans = JOptionPane.showConfirmDialog(handle.frame.getContentPane(), host + " wants to start a game with you. Accept?", "Game Request", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,new ImageIcon(ImageFactory.qMark));

			if(ans == 0) // Request Accepted
			{
				System.out.println(host);
				client = new Client(host, this);
				client.sendMessage(acceptStart);
				for(int i = 0; i < 10000; i++);
				startNewGame();
			}
			else
			{
				client = new Client(host, this);
				client.sendMessage(refuseStart);
			}
			return;

		case AcceptStart:
			System.out.println("Request Accepted");
			startNewGame();
			return;

		case RefuseStart:
			System.out.println("Request Refused");
			return;

		case Broadcast:
			String address = msg.split(" ")[1];
			System.out.println("broadcast message from: " + address);
			Client c = new Client(address, this);
			c.sendMessage(broadcastResponse);
			addIP(address);
			return;

		case BroadcastResponse:
			addIP(msg.split(" ")[1]);
			return;

		default:
			System.out.println("troll message received");

		}

	}


	enum MessageType
	{
		Start,
		AcceptStart,
		RefuseStart,
		Broadcast,
		BroadcastResponse,
		Other
	}

	MessageType messageType(String msg)
	{
		String[] args = msg.split(" ");

		if (args[0].equals("start"))
			return MessageType.Start;

		if(args[0].equals(acceptStart))
			return MessageType.AcceptStart;

		if(args[0].equals(refuseStart))
			return MessageType.RefuseStart;

		if(args[0].equals("broadcast"))
			return MessageType.Broadcast;

		if(args[0].equals("broadcastresponse"))
			return MessageType.BroadcastResponse;

		return MessageType.Other;
	}


	int compare(String ip1, String ip2)
	{
		String[] comps1 = ip1.split("\\.");
		String[] comps2 = ip2.split("\\.");
		for(int i = 0; i < 4; i++)
			if(!comps1[i].equals(comps2[i]))
				return Integer.parseInt(comps2[i]) - Integer.parseInt(comps1[i]);

		return 0;
	}

	private synchronized void addIP(String host)
	{
		
		int i = 0;
		for(i = 0; i < ips.size() && compare(ips.get(i), host) > 0; i++);

		ips.add(i, host);
		ipTable.updateUI();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void setVisible(boolean visible)
	{
		super.setVisible(visible);
		if (visible)
		{
			server = new Server(this);
			server.start();
			getIPs();
		}
		else
		{
			ips.clear();
			if(!gameRun)
			{
				server.close();
				gameRun = false;
			}
			if(ipThread != null && ipThread.isAlive())
				ipThread.stop();
		}
	}
	
	@SuppressWarnings("deprecation")
	private void getIPs()
	{
		if(ipThread != null && ipThread.isAlive())
		{
			ipThread.stop();
			ips.clear();
		}

		final String routerIP = routerIP();

		final int timeout = 100;

		if(routerIP == null)
		{
			Thread thread = new Thread(new Runnable()
			{

				@Override
				public void run()
				{

					for (int i = 1; i < 255; i++)
					{
						String host = "192.168.1." + i;
						try
						{
							if (!host.equals(localHost) && InetAddress.getByName(host).isReachable(timeout))
								addIP(host);
						}
						catch(Exception e)
						{

						}

					}
				}
			});
			thread.start();
			ipThread = thread;
		}
		else
		{
			Thread thread = new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					String[] comps = routerIP.split("\\.");
					int[] intComps = new int[4];

					for(int i = 0; i < 4; i++)
						intComps[i] = Integer.parseInt(comps[i]);

					for(int a = intComps[0]; a < 255; a++)
					{
						for(int b =  a > intComps[0] ? 0 : intComps[1]; b < 255; b++)
						{
							for(int c =  a > intComps[0] || b > intComps[1] ? 0 : intComps[2]; c < 255; c++)
							{
								for(int d = a > intComps[0] || b > intComps[1] || c > intComps[2] ? 0 : intComps[3]; d < 255; d++)
								{
									String host = a + "." + b + "." + c +"." + d;
									try
									{
										if (!host.equals(localHost) && InetAddress.getByName(host).isReachable(timeout))
											addIP(host);
									}
									catch(Exception e)
									{

									}
								}
							}
						}
					}
				}
			});
			thread.start();
			ipThread = thread;
		}




	}

	private String routerIP()
	{
		class IPGetter implements Callable<String>
		{

			@Override
			public String call() throws Exception
			{
				if(System.getProperty("os.name").startsWith("Windows"))
					return null;

				Process result = Runtime.getRuntime().exec("traceroute -m 1 www.google.com");

				BufferedReader output = new BufferedReader(new InputStreamReader(result.getInputStream()));
				String thisLine = output.readLine();
				StringTokenizer st = new StringTokenizer(thisLine);
				st.nextToken();
				String gateway = st.nextToken();
				return gateway;
			}

		}

		String result = null;

		try
		{
			ExecutorService service = Executors.newSingleThreadExecutor();
			result = service.submit(new IPGetter()).get(500, TimeUnit.MILLISECONDS);
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		return result;

	}


}
