package Menus;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import Effects.Sound;



public class PreferencesController
{
	private Preferences prefs = Preferences.userRoot().node(this.getClass().getName());

	private static final String musicVolume = "MDBMusicVolume";
	private static final String soundEffectVolume = "MDBSoundEffectVolume";


	public void setMusicVolume(double volume)
	{
		try
		{
			prefs.putDouble(musicVolume, volume);
			Sound.music.setVolume( (float) volume);
		}
		catch(Exception e)
		{
			System.out.println("Couldn't set prefered Music Volume");
		}
	}

	public double getMusicVolume()
	{
		try
		{
			return prefs.getDouble(musicVolume, 0.8);
		}
		catch(Exception e)
		{
			System.out.println("Couldn't retrieve prefered Music Volume");
			return 0.8;
		}
	}

	public void setSoundEffectVolume(double volume)
	{
		try
		{
			prefs.putDouble(soundEffectVolume, volume);
			for(Sound s : Sound.soundEffects)
				s.setVolume((float) volume);
		}
		catch(Exception e)
		{
			System.out.println("Couldn't set prefered Sound Effect Volume");
		}
	}

	public double getSoundEffectVolume()
	{
		try
		{
			return prefs.getDouble(soundEffectVolume, 0.8);
		}
		catch(Exception e)
		{
			System.out.println("Couldn't retrieve prefered Sound Effect Volume");
			return 0.8;
		}
	}


	public void reset()
	{
		try
		{
			prefs.clear();
		}
		catch (BackingStoreException e)
		{
			System.out.println("Error resetting preferences: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
