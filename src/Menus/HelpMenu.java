package Menus;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Effects.ImageFactory;
import Menus.CustomUI.Button;
import Menus.CustomUI.Menu;

public class HelpMenu extends Menu
{
	private static final long serialVersionUID = 1L;
	
	JLabel explosionLabel;
	JLabel extraLifeLabel;
	
	JLabel fastForwardLabel;
	JLabel fastBackwardLabel;
	
	JLabel shrinkLabel;
	JLabel enlargeLabel;
	
	JLabel descriptionLabel;
	
	Button backButton;
	
	
	public HelpMenu(final MenuHandler handle)
	{
		setLayout(null);
		
		Dimension s = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension center = new Dimension(s.width/2, s.height/2);
		
		backButton = new Button("Back");
		backButton.setBounds(10, (int) s.getHeight() - 110, 100, 50);
		backButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				handle.show(MenuHandler.mainMenuId);
			}
		});
		add(backButton);
		
		
		
		
		
		String text = "<html>Glow is a classic brick breaker game.<br>";
		text += "The rules are simple: break all the bricks by throwing the ball<br>";
		text += "and don't let the ball fall down. If you do, you lose a life and<br>";
		text += "after your lifes are over, the game is over. Some broken bricks give<br> a power-up which will make the game easier or<br>";
		text += "harder. Each broken bricks gives you 10 points. You can choose to save<br>";
		text += "your score to your computer, upload them to the server, both, or none.";
		
		
		descriptionLabel = new JLabel(text);
		descriptionLabel.setForeground(Color.WHITE);
		descriptionLabel.setBounds(0, 50, s.width, 100);
		add(descriptionLabel);
		
		
		explosionLabel = new JLabel();
		explosionLabel.setForeground(Color.WHITE);
		explosionLabel.setBounds(0, (int) descriptionLabel.getBounds().getMaxY() + 100, s.width, 40);
		explosionLabel.setHorizontalAlignment(JLabel.CENTER);
		
		explosionLabel.setIcon( new ImageIcon(ImageFactory.explosionPowerUp));
		text = "<html>When the explosion power-up occures, all bricks<br>in a defined range of the ball will be destroyed.<html>";
		explosionLabel.setText(text);
		add(explosionLabel);
		
		extraLifeLabel = new JLabel();
		extraLifeLabel.setForeground(Color.WHITE);
		extraLifeLabel.setBounds(0, (int) explosionLabel.getBounds().getMaxY() + 10, s.width, 40);
		extraLifeLabel.setHorizontalAlignment(JLabel.CENTER);
		
		extraLifeLabel.setIcon(new ImageIcon(ImageFactory.extraLifePowerUp));
		text = "<html>When picked-up, the extra life power-up will provide<br>";
		text += "an extra life for the game to continue<html>";
		extraLifeLabel.setText(text);
		add(extraLifeLabel);
		
		
		
		fastForwardLabel = new JLabel();
		fastForwardLabel.setForeground(Color.WHITE);
		fastForwardLabel.setBounds(0, (int) extraLifeLabel.getBounds().getMaxY() + 10, s.width, 40);
		fastForwardLabel.setHorizontalAlignment(JLabel.CENTER);
		
		fastForwardLabel.setIcon(new ImageIcon(ImageFactory.fastenPowerUp));
		text = "<html>The fast forward power-up increases the speed of<br>";
		text += "the ball upon picking it up";
		fastForwardLabel.setText(text);
		add(fastForwardLabel);
		
		
		
		fastBackwardLabel = new JLabel();
		fastBackwardLabel.setForeground(Color.WHITE);
		fastBackwardLabel.setBounds(0, (int) fastForwardLabel.getBounds().getMaxY() + 10, s.width, 40);
		fastBackwardLabel.setHorizontalAlignment(JLabel.CENTER);
		
		fastBackwardLabel.setIcon(new ImageIcon(ImageFactory.slowdownPowerUp));
		text = "<html>When the fastbackward power-up is picked,<br>";
		text += "the ball's speed decreases by a certain factor.<html>";
		fastBackwardLabel.setText(text);
		add(fastBackwardLabel);
		
		
		
		shrinkLabel = new JLabel();
		shrinkLabel.setForeground(Color.WHITE);
		shrinkLabel.setBounds(0, (int) fastBackwardLabel.getBounds().getMaxY() + 10, s.width, 40);
		shrinkLabel.setHorizontalAlignment(JLabel.CENTER);
		
		shrinkLabel.setIcon(new ImageIcon(ImageFactory.shrinkPowerUp));
		text = "<html>When the shrink power-up is picked-up, the paddle's<br>";
		text += "size is shrank by a certain factor.<html>";
		shrinkLabel.setText(text);
		add(shrinkLabel);
		
		
		enlargeLabel = new JLabel();
		enlargeLabel.setForeground(Color.WHITE);
		enlargeLabel.setBounds(0, (int) shrinkLabel.getBounds().getMaxY() + 10, s.width, 40);
		enlargeLabel.setHorizontalAlignment(JLabel.CENTER);
		
		enlargeLabel.setIcon(new ImageIcon(ImageFactory.enlargePowerUp));
		text = "<html>The enlarge power-up increases the paddle's size by<br>";
		text += "a certain factor when picked-up";
		enlargeLabel.setText(text);
		add(enlargeLabel);
		
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Graphics2D g2d= (Graphics2D) g;
		FontMetrics metrics = g2d.getFontMetrics(explosionLabel.getFont());
		
		JLabel[] labels = {explosionLabel, extraLifeLabel, fastBackwardLabel, fastForwardLabel, shrinkLabel, enlargeLabel};
		
		int stringSize = metrics.stringWidth(explosionLabel.getText());
		
		int width = Toolkit.getDefaultToolkit().getScreenSize().width;
		int x = width/2 - stringSize/4;
		
		for(JLabel label : labels)
		{
			label.setBounds(x, (int) label.getBounds().y, (int) label.getBounds().getWidth(), (int) label.getBounds().getHeight());
			label.setHorizontalAlignment(JLabel.LEFT);
		}
		

		descriptionLabel.setBounds(explosionLabel.getBounds().x, 50, 1000, 200);
		
	}
	
	
	
	
	
	
	
}
