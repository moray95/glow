package Menus;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import Game.Game;
import Menus.CustomUI.Button;
import Menus.CustomUI.Menu;

public class LevelCompleteMenu extends Menu
{
	private static final long serialVersionUID = 1L;
	int level;
	JLabel label;
	int score;
	
	public LevelCompleteMenu(final MenuHandler handle) 
	{
		
		setLayout(null);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension center = new Dimension(screenSize.width/2, screenSize.height/2);
		
		
		label = new JLabel("<html><div style=\"text-align: center;\">Level " + level + " completed!" + "<br>Your score: " + score + "</html>");
		label.setBounds(0, center.height - 80, screenSize.width, 100);
		label.setForeground(Color.WHITE);
		label.setHorizontalAlignment(JLabel.CENTER);
		label.setFont(new Font(label.getFont().getFontName(), label.getFont().getStyle(), 20));
		
		Button button = new Button("Continue");
		button.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					Thread.sleep(1000);
				} 
				catch (InterruptedException ex)
				{
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}
				
				handle.setGame(new Game(handle));
				handle.getGame().level = level;
				handle.getGame().score.score = score;
				handle.optionsMenu.oldMenu = MenuHandler.newGameId;

				handle.show(MenuHandler.newGameId);
				new Thread(new Runnable()
				{
					
					@Override
					public void run()
					{
						handle.getGame().run();
					}
				}).start();
			}
		});
		button.setBounds(center.width - 50, (int) label.getBounds().getMaxY() + 10, 100, 50);
		
		add(label);
		add(button);
		
		
	}
	
	public void setScore(int score)
	{
		this.score = score;
		label.setText("<html><div style=\"text-align: center;\">Level " + level + " completed!" + "<br>Your score: " + score + "</html>");
	}
	
	public void setLevel(int level)
	{
		this.level = level;
		label.setText("<html><div style=\"text-align: center;\">Level " + level + " completed!" + "<br>Your score: " + score + "</html>");
	}
}
