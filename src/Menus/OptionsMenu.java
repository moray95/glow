package Menus;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Menus.CustomUI.Button;
import Menus.CustomUI.Menu;

class OptionsMenu extends Menu
{
	private static final long serialVersionUID = 1L;

	MenuHandler handle;

	Button backButton = new Button("Back");
	JSlider musicVolumeSlider = new JSlider(0, 100, 1);
	JSlider soundEffectSlider = new JSlider(0, 100, 1);
	Button resetSettingsButton = new Button("Reset Settings");

	PreferencesController prefs = new PreferencesController();
	String oldMenu;
	
	public OptionsMenu(final MenuHandler handle)
	{
		super();

		setLayout(null);

		JLabel musicVolumeLabel = new JLabel("Music Volume", JLabel.RIGHT);
		JLabel soundEffectLabel = new JLabel("Sound Effects Volume", JLabel.RIGHT);
		musicVolumeLabel.setVerticalAlignment(JLabel.CENTER);
		soundEffectLabel.setVerticalAlignment(JLabel.CENTER);
		
		musicVolumeSlider.setValue((int) (prefs.getMusicVolume() * 100));
		soundEffectSlider.setValue((int) (prefs.getSoundEffectVolume() * 100));

		musicVolumeSlider.setOpaque(false);
		musicVolumeLabel.setForeground(Color.WHITE);
		soundEffectSlider.setOpaque(false);
		soundEffectLabel.setForeground(Color.WHITE)
		;
		add(musicVolumeLabel);
		add(musicVolumeSlider);
		add(soundEffectLabel);
		add(soundEffectSlider);
		add(backButton);
		add(resetSettingsButton);


		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension center = new Dimension(screenSize.width / 2, screenSize.height / 2);

		musicVolumeLabel.setBounds((int) center.getWidth() - 160, 10, 150, 50);
		setBoundsRight(musicVolumeSlider, musicVolumeLabel);
		setBoundsLeft(soundEffectLabel, musicVolumeLabel);
		setBoundsRight(soundEffectSlider, soundEffectLabel);

		backButton.setBounds(10, screenSize.height - 110, 100, 50);
		resetSettingsButton.setBounds(screenSize.width - 110, screenSize.height - 110, 100, 50);


		// Action Listeners

		musicVolumeSlider.addChangeListener(new ChangeListener()
		{

			@Override
			public void stateChanged(ChangeEvent e)
			{
				prefs.setMusicVolume(musicVolumeSlider.getValue()/100.0);
			}
		});

		soundEffectSlider.addChangeListener(new ChangeListener()
		{

			@Override
			public void stateChanged(ChangeEvent e)
			{
				prefs.setSoundEffectVolume(soundEffectSlider.getValue()/100.0);
			}
		});

		backButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				handle.show(oldMenu);
			}
		});

		resetSettingsButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				prefs.reset();
				musicVolumeSlider.setValue((int) (prefs.getMusicVolume() * 100));
				soundEffectSlider.setValue((int) (prefs.getSoundEffectVolume() * 100));
			}
		});
	}

	private void setBoundsRight(JComponent comp, JComponent left)
	{
		Dimension center = Toolkit.getDefaultToolkit().getScreenSize();
		center.setSize(center.width / 2, center.height / 2);
		comp.setBounds((int) center.width + 10, (int) left.getBounds().getMinY(), 150, 50);
	}

	private void setBoundsLeft(JComponent comp, JComponent top)
	{
		comp.setBounds((int) top.getBounds().getMinX(), (int) top.getBounds().getMaxY() + 10, 150, 50);
	}

}