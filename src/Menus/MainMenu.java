package Menus;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import Game.Game;
import Menus.CustomUI.Button;
import Menus.CustomUI.Menu;

class MainMenu extends Menu
{
	private static final long serialVersionUID = 1L;

	Button newGameButton = new Button("Play");
	Button helpMenuButton = new Button("Help");
	Button optionsMenuButton = new Button("Options");
	Button exitGameButton = new Button("Exit");
	Button highScoresButton = new Button("High Scores");

	MenuHandler handle;

	public MainMenu(final MenuHandler handle)
	{
		super();
		setLayout(null);

		this.handle = handle;


		add(newGameButton);
		add(helpMenuButton);
		add(highScoresButton);
		add(optionsMenuButton);
		add(exitGameButton);


		Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
		Dimension center = new Dimension(screenSize.width / 2, screenSize.height / 2);

		newGameButton.setBounds(10, center.height /* 310/2*/, 100, 50);
		setBounds(helpMenuButton, newGameButton);
		setBounds(highScoresButton, helpMenuButton);
		setBounds(optionsMenuButton, highScoresButton);
		setBounds(exitGameButton, optionsMenuButton);

		// ActionListeners

		newGameButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				System.out.println("New Game");
				
				
				handle.setGame(new Game(handle));
				handle.pauseMenu.game = handle.getGame();
				
				handle.show(MenuHandler.newGameId);
				
				new Thread(new Runnable()
				{
					
					@Override
					public void run()
					{
						handle.getGame().run();
					}
				}).start();
				
				System.out.println("Game started");
			}
		});

		helpMenuButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				handle.show(MenuHandler.helpMenuId);
			}
		});

		highScoresButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				System.out.println("High Scores");
				handle.show(MenuHandler.highScoresMenuId);

			}
		});

		optionsMenuButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				handle.show(MenuHandler.optionsMenuId);
			}
		});

		exitGameButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		});
	}

	private void setBounds(JButton button, JButton previous)
	{
		button.setBounds((int) previous.getBounds().getMinX() + 15,
				(int) previous.getBounds().getMaxY() + 15, 100, 50);
	}
}