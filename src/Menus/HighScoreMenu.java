package Menus;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import HighScore.DBHighScore;
import HighScore.HighScore;
import HighScore.HighScoreConnectionDelegate;
import HighScore.LocalHighScore;
import Menus.CustomUI.Button;
import Menus.CustomUI.Menu;

class HighScoreMenu extends Menu implements HighScoreConnectionDelegate
{
	private static final long serialVersionUID = 1L;
	JTable localTableView;
	JTable dbTableView;
	
	Button backButton = new Button("Back");
	Button reloadDBHSButton = new Button("Reload Scores");
	Button resetLocalHSButton = new Button("Reset Scores");
	
	ArrayList<HighScore> dbHighScores = new ArrayList<HighScore>();
	
	final MenuHandler handle;
	
	public HighScoreMenu(final MenuHandler handle)
	{
		super();
		
		this.handle = handle;
		
		setLayout(null);
		
		
		localTableView = new JTable(new TableModel()
		{
			
			@Override
			public void setValueAt(Object aValue, int rowIndex, int columnIndex)
			{
				
			}
			
			@Override
			public void removeTableModelListener(TableModelListener l)
			{				
			}
			
			@Override
			public boolean isCellEditable(int rowIndex, int columnIndex)
			{
				return false;
			}
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex)
			{
				switch(columnIndex)
				{
				case 0:
					return LocalHighScore.getHighScores().get(rowIndex).name;
				case 1:
					return LocalHighScore.getHighScores().get(rowIndex).score;
				default:
					return null;
				}
				
			}
			
			@Override
			public int getRowCount()
			{
				return LocalHighScore.getHighScores().size();
			}
			
			@Override
			public String getColumnName(int columnIndex)
			{
				return columnIndex == 0 ? "Name" : "Score";
			}
			
			@Override
			public int getColumnCount()
			{
				return 2;
			}
			
			@Override
			public Class<?> getColumnClass(int columnIndex)
			{
				return HighScore.class;
			}
			
			@Override
			public void addTableModelListener(TableModelListener l)
			{	
			}
		});
		dbTableView = new JTable(new TableModel()
		{
			
			@Override
			public void setValueAt(Object aValue, int rowIndex, int columnIndex)
			{
			}
			
			@Override
			public void removeTableModelListener(TableModelListener l)
			{
			}
			
			@Override
			public boolean isCellEditable(int rowIndex, int columnIndex)
			{
				return false;
			}
			
			@Override
			public Object getValueAt(int rowIndex, int columnIndex)
			{
				switch(columnIndex)
				{
				case 0:
					return dbHighScores.get(rowIndex).name;
				case 1:
					return dbHighScores.get(rowIndex).score;
				default:
					return null;
				}
			}
			
			@Override
			public int getRowCount()
			{
				return dbHighScores.size();
			}
			
			@Override
			public String getColumnName(int columnIndex)
			{
				switch(columnIndex)
				{
				case 0:
					return "Name";
				case 1:
					return "Score";
				default:
					return null;
				}
			}
			
			@Override
			public int getColumnCount()
			{
				return 2;
			}
			
			@Override
			public Class<?> getColumnClass(int columnIndex)
			{
				return TableColumn.class;
			}
			
			@Override
			public void addTableModelListener(TableModelListener l)
			{
			}
		});
		
	
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension center = new Dimension(screenSize.width / 2, screenSize.height / 2);
		
		JScrollPane localHSScrollPane = new JScrollPane(localTableView);
		JScrollPane dbHSScrollPane = new JScrollPane(dbTableView);
		
		localHSScrollPane.setBounds(10, 50, center.width - 20, screenSize.height - 200);
		dbHSScrollPane.setBounds(center.width + 10, 50, center.width - 20, screenSize.height - 200);
		
		backButton.setBounds(10, (int) (screenSize.getHeight() - 110), 100, 50);
		
		backButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				handle.show(MenuHandler.mainMenuId);
			}
		});
		
		resetLocalHSButton.setBounds( (int) localHSScrollPane.getBounds().getMaxX() - 100, (int) backButton.getBounds().getMinY(), 100, 50);
		reloadDBHSButton.setBounds( (int) dbHSScrollPane.getBounds().getMaxX() - 100, (int) backButton.getBounds().getMinY(), 100, 50);
		
		resetLocalHSButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				LocalHighScore.resetHighScores();
				localTableView.updateUI();
			}
		});
		
		reloadDBHSButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DBHighScore.getHighScores();
			}
		});
		
		
		JLabel localLabel = new JLabel("Local Scores", JLabel.CENTER);
		localLabel.setBounds( (int) localHSScrollPane.getBounds().getMinX(), 20, (int) localHSScrollPane.getBounds().getWidth(), 20);
		localLabel.setForeground(Color.WHITE);
		
		JLabel globalLabel = new JLabel("Global Scores", JLabel.CENTER);
		globalLabel.setBounds( (int) dbHSScrollPane.getBounds().getMinX(), 20, (int) dbHSScrollPane.getBounds().getWidth(), 20);
		globalLabel.setForeground(Color.WHITE);
		
		add(localHSScrollPane);
		add(dbHSScrollPane);
		add(backButton);
		add(resetLocalHSButton);
		add(reloadDBHSButton);
		add(localLabel);
		add(globalLabel);
		
		DBHighScore.delegate = this;
		DBHighScore.getHighScores();
	}

	@Override
	public void didUploadHighScore(HighScore hs, boolean success)
	{
		
	}

	@Override
	public void didDownloadHighScores(ArrayList<HighScore> highScores)
	{
		if(highScores != null)
		{
			
			dbHighScores = highScores;
			dbTableView.updateUI();
		}
	}
	
	@Override
	public
	void setVisible(boolean visible)
	{
		super.setVisible(visible);
		if(visible)
		{
			localTableView.updateUI();
			repaint();
			
		}
	}
	
}
