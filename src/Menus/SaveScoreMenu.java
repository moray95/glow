package Menus;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import Effects.ImageFactory;
import Game.Game;
import HighScore.DBHighScore;
import HighScore.HighScore;
import HighScore.LocalHighScore;
import Menus.CustomUI.Button;
import Menus.CustomUI.Menu;
import Menus.CustomUI.PlaceholderTextField;

public class SaveScoreMenu extends Menu
{
	private static final long serialVersionUID = 1L;
	
	MenuHandler handle;
	int score;
	JLabel scoreLabel;
	
	PlaceholderTextField nameField;
	JCheckBox localSaveBox;
	JCheckBox dbSaveBox;
	
	public SaveScoreMenu(final MenuHandler handle)
	{
		this.handle = handle;
		
		setLayout(null);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension center = new Dimension(screenSize.width/2, screenSize.height/2);
		
		scoreLabel = new JLabel("Your score: " + score);
		scoreLabel.setForeground(Color.WHITE);
		scoreLabel.setBounds(0, center.height -40, screenSize.width, 20);
		scoreLabel.setHorizontalAlignment(JLabel.CENTER);
		
		
		nameField = new PlaceholderTextField("Name");
		nameField.setBounds(center.width - 100, (int) scoreLabel.getBounds().getMaxY() + 10, 200, 20); 
		
		
		localSaveBox = new JCheckBox("Save to local scores");
		localSaveBox.setForeground(Color.WHITE);
		localSaveBox.setBounds( (int) nameField.getBounds().x, (int) nameField.getBounds().getMaxY() + 10, 200, 20);
		localSaveBox.setOpaque(false);
		
		dbSaveBox = new JCheckBox("Upload to database");
		dbSaveBox.setForeground(Color.WHITE);
		dbSaveBox.setBounds( (int) localSaveBox.getBounds().x, (int) localSaveBox.getBounds().getMaxY() +10, 200, 20);
		dbSaveBox.setOpaque(false);
		
		Button mainMenuButton = new Button("Main Menu");
		mainMenuButton.setBounds(screenSize.width - 110, screenSize.height - 110, 100, 50);
		mainMenuButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(saveScore())
					handle.show(MenuHandler.mainMenuId);
				else
					System.out.println("Coudln't save score");
			}
		});
		
		
		Button restartButton = new Button("Restart Game");
		restartButton.setBounds(10, (int) screenSize.getHeight() - 110, 100, 50);
		restartButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				handle.setGame(new Game(handle));
				handle.show(MenuHandler.newGameId);
				new Thread(new Runnable()
				{
					
					@Override
					public void run()
					{
						handle.getGame().run();
					}
				}).start();
			}
		});
		
		
		add(nameField);
		add(scoreLabel);
		add(localSaveBox);
		add(dbSaveBox);
		add(mainMenuButton);
		add(restartButton);
	}
	
	boolean saveScore()
	{
		if((localSaveBox.isSelected() || dbSaveBox.isSelected()) &&  nameField.getText().isEmpty())
		{
			JOptionPane.showMessageDialog(handle.frame.getContentPane(), "Please enter a valid name.", "Invalid name", JOptionPane.OK_OPTION, new ImageIcon(ImageFactory.errorImage));
			return false;
		}
		if(localSaveBox.isSelected())
		{
			LocalHighScore.addHighScores(new HighScore(nameField.getText(), getScore()));
		}
		
		if(dbSaveBox.isSelected())
		{
			DBHighScore.uploadScoreToDatabase(new HighScore(nameField.getText(), getScore()));
		}
		return true;
	}
	
	public int getScore()
	{
		return score;
	}
	
	public void setScore(int score)
	{
		this.score = score;
		scoreLabel.setText("Your score: " + score);
	}
	
}
