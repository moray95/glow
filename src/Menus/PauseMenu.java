package Menus;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Game.Game;
import Menus.CustomUI.Button;
import Menus.CustomUI.Menu;

public class PauseMenu extends Menu
{
	private static final long serialVersionUID = 1L;
	
	MenuHandler handle;
	public Game game;
	public Thread gameThread;
	
	public PauseMenu(final MenuHandler handle)
	{
		super();
		
		setLayout(null);
		
		this.handle = handle;
		
		Button resumeButton = new Button("Resume");
		Button newGameButton = new Button("New Game");
		Button optionsMenuButton = new Button("Options");
		Button mainMenuButton = new Button("Main Menu");
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension center = new Dimension(screenSize.width/2, screenSize.height/2);
		
		resumeButton.setBounds(center.width - 50, center.height - 115, 100, 50);
		newGameButton.setBounds((int) resumeButton.getBounds().x, (int) resumeButton.getBounds().getMaxY() + 10, 100, 50);
		optionsMenuButton.setBounds((int) newGameButton.getBounds().x, (int) newGameButton.getBounds().getMaxY() + 10, 100, 50);
		mainMenuButton.setBounds((int) optionsMenuButton.getBounds().x, (int) optionsMenuButton.getBounds().getMaxY() + 10, 100, 50);
		
		
		resumeButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				handle.getGame().paused = false;
				handle.show(MenuHandler.newGameId);
				synchronized(game)
				{
					game.notify();
				}
			}
		});
		newGameButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				handle.getGame().stopped = true;
				synchronized(game)
				{
					game.notify();
				}
				handle.setGame(new Game(handle));
				game = handle.getGame();
				handle.show(MenuHandler.newGameId);
				new Thread(new Runnable()
				{
					
					@Override
					public void run()
					{
						game.run();
					}
				}).start();
				
				
			}
		});
		optionsMenuButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				handle.optionsMenu.oldMenu = MenuHandler.pauseMenuId;
				handle.show(MenuHandler.optionsMenuId);
			}
		});
		mainMenuButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				synchronized (game)
				{
					game.notify();
				}
				handle.getGame().stopped = true;
				handle.optionsMenu.oldMenu = MenuHandler.mainMenuId;
				handle.show(MenuHandler.mainMenuId);
				
				handle.setGame(new Game(handle));
				game = handle.getGame();
				
			}
		});
		
		add(resumeButton);
		add(newGameButton);
		add(optionsMenuButton);
		add(mainMenuButton);
		
	}
}